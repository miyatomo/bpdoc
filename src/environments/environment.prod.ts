export const AppConfig = {
  production: true,
  environment: 'PROD',
  bpReleaseDirPath: './BPReleaseSources',
};
