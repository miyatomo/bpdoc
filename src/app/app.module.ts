import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './services/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';
import { GeneratorComponent } from './pages/generator/generator.component';
import { FileDropModule } from 'ngx-file-drop';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';

import { ObjectDetailComponent } from './pages/object-detail/object-detail.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { TemplateHomeComponent } from './pages/templatehome/templatehome.component';
import { ObjectListComponent } from './pages/object-list/object-list.component';
import { GroupsComponent } from './pages/groups/groups.component';
import { SideNavComponent } from './components/container/side-nav/side-nav.component';
import { HeaderComponent } from './components/container/header/header.component';
import { SearchComponent } from './components/presantation/search/search.component';
import { TagComponent } from './components/presantation/tag/tag.component';
import { DescriptionComponent } from './components/presantation/description/description.component';
import { BpObjectComponent } from './components/presantation/bp-object/bp-object.component';
import { MetaDataComponent } from './components/presantation/meta-data/meta-data.component';
import { PageListComponent } from './components/container/page-list/page-list.component';
import { TabComponent } from './components/presantation/tab/tab.component';
import { TableComponent } from './components/presantation/table/table.component';
import { GroupContentsComponent } from './components/container/group-contents/group-contents.component';
import { ObjectContentsComponent } from './components/container/object-contents/object-contents.component';
import { MenuBtnComponent } from './components/presantation/menu-btn/menu-btn.component';
import { TabContentsComponent } from './components/container/tab-contents/tab-contents.component';
import { TabNavComponent } from './components/presantation/tab-nav/tab-nav.component';
import { DetailInformationComponent } from './components/presantation/detail-information/detail-information.component';
import { DetailInputsComponent } from './components/presantation/detail-inputs/detail-inputs.component';
import { DetailOutputsComponent } from './components/presantation/detail-outputs/detail-outputs.component';
import { ParamsComponent } from './components/presantation/params/params.component';
import { ObjectContentsDetailComponent } from './components/container/object-contents-detail/object-contents-detail.component';
import { NavInternalComponent } from './components/container/nav-internal/nav-internal.component';
import { SearchResultComponent } from './pages/search-result/search-result.component';
import { CopyButtonComponent } from './components/presantation/copy-button/copy-button.component';
import { PackagesComponent } from './pages/packages/packages.component'

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    WebviewDirective,
    PackagesComponent,
    GeneratorComponent,
    AppComponent,
    GroupsComponent,
    ObjectDetailComponent,
    NotFoundComponent,
    ObjectListComponent,
    SideNavComponent,
    HeaderComponent,
    SearchComponent,
    TagComponent,
    DescriptionComponent,
    BpObjectComponent,
    MetaDataComponent,
    PageListComponent,
    TabComponent,
    TableComponent,
    GroupContentsComponent,
    ObjectContentsComponent,
    MenuBtnComponent,
    TabContentsComponent,
    TabNavComponent,
    DetailInformationComponent,
    DetailInputsComponent,
    DetailOutputsComponent,
    ParamsComponent,
    ObjectContentsDetailComponent,
    NavInternalComponent,
    TemplateHomeComponent,
    SearchResultComponent,
    CopyButtonComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    FileDropModule,
    ReactiveFormsModule,
  ],
  providers: [ElectronService],
  bootstrap: [AppComponent]
})
export class AppModule { }
