import UUID from 'uuid';

import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router'
import { BpObject } from '../../models/bp_object';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
  @Input()
  public searchResultObjects: BpObject[] = [];  
  @Input()
  public searchResultPageObjects: BpObject[] = [];
 
  constructor(public router :Router) { }

  ngOnInit() {
  }
}
