import UUID from 'uuid';

import { Component, OnInit } from '@angular/core';
import { ObjectService } from '../../../../src/app/services/object.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BpObject } from '../../../../src/app/models/bp_object';
import { GroupService } from '../../../../src/app/services/group.service';

// FIXME:コンテナに寄せる
@Component({
  selector: 'app-object-detail',
  templateUrl: './object-detail.component.html',
  styleUrls: ['./object-detail.component.css']
})
export class ObjectDetailComponent implements OnInit {

  public id?: string;
  public bpObj?: BpObject;
  public groups?: string[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private bpObjService: ObjectService,
    private groupService: GroupService,
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    
    if (!this.id) {
      this.router.navigate(['notfound']);
      return;
    }

    this.bpObj = this.bpObjService.getObjectById(this.id);
    if (!this.bpObj) {
      this.router.navigate(['notfound']);
      return;
    }
    this.bpObj.functions.forEach(fun => {
      // TODO:deprecated以外の属性が追加された場合はフラグをクラスにして、Generatorの段階で変換するようにする
      fun.d_deprecated = false;
      if(fun.description.includes('@deprecated')){
        fun.d_deprecated = true;
        fun.description = fun.description.replace('@deprecated','');
      }
    });

    this.groups = this.bpObj.group.map(item => (this.groupService.getGroupNameFromId(item).replace(/\//g, ' > ') + ' > ' + this.bpObj.name));
  }
}
