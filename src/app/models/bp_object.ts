export class BpObject {
    id: string;
    name: string;
    group: string[];
    description: string;
    runmode: string;
    language: string;
    globalCode: string;
    code: string;
    references: string[];
    functions: {
        id: string,
        name: string,
        public: boolean,
        d_deprecated: boolean,
        description: string,
        precondition: string[],
        postcondition: string[],
        inputs: {
        type: string,
        name: string,
        description?: string,
        }[];
        outputs: {
        type: string,
        name: string,
        description?: string,
        }[];
        dependencies: {
        name: string,
        type: string
        }[];
    }[]
}
