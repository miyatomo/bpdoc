export class SuggestMsg {
    id: string;
    type: string;
    description: string;
    name: string;
}
