export class Param {
    type: string;
    name: string;
    description: string;
}