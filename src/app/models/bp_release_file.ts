import { BpGroup } from './bp_group';
import { BpObject } from '../models/bp_object';

export type BpReleaseFileMeta = {
    releaseName: string,
    packageId: string,
    packageName: string,
    createdDate: Date,
    createdBy: string,
}

export type BpReleaseFile = {
    meta: BpReleaseFileMeta,
    groups: BpGroup[];
    objects: BpObject[];
}
