import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneratorComponent } from './pages/generator/generator.component';
import { TemplateHomeComponent } from './pages/templatehome/templatehome.component';
import { GroupsComponent } from './pages/groups/groups.component';
import { PackagesComponent } from './pages/packages/packages.component';
import { ObjectListComponent } from './pages/object-list/object-list.component';
import { ObjectDetailComponent } from './pages/object-detail/object-detail.component';
import { SearchResultComponent } from './pages/search-result/search-result.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';


const routes: Routes = [
    // { path: '', component: HomeComponent },
    { path: '', component: TemplateHomeComponent },
    { path: 'generator', component: GeneratorComponent },
    { path: 'template', component: TemplateHomeComponent },
    { path: 'groups', component: GroupsComponent },
    { path: 'packages', component: PackagesComponent },
    { path: 'objects', component: ObjectListComponent },
    { path: 'object/:id', component: ObjectDetailComponent },
    { path: 'searchResult', component: SearchResultComponent },
    // { path: '**', component: NotFoundComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
