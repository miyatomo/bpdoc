import { Injectable } from '@angular/core';
import { BpGroup } from '../models/bp_group';
import { BpReleaseReaderService } from './bp-release-reader.service'

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private bpReleaseReader:BpReleaseReaderService) { }

  private get templates() : BpGroup[]  {
    return this.bpReleaseReader.getAllGroupTemplates();
  }

  public getAllGroupId(): string[] {
    return this.templates.map(item => item.id);
  }

  public getAllGroup(): BpGroup[] {
    return this.templates;
  }

  public getGroupNameFromId(id: string): string {
    const findGrp = this.templates.find(f => f.id === id);
    if(findGrp) {
      return findGrp.name
    } else {
      return '';
    }
  }

  public getMembersFromId(id: string): string[] {
    const findGrp = this.templates.find(f => f.id === id);
    if(findGrp) {
      return findGrp.members
    } else {
      return [];
    }
  }
}
