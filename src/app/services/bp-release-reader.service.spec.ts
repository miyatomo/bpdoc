import { TestBed } from '@angular/core/testing';

import { BpReleaseReaderService } from './bp-release-reader.service';

describe('BpReleaseReaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BpReleaseReaderService = TestBed.get(BpReleaseReaderService);
    expect(service).toBeTruthy();
  });
});
