import { Injectable } from '@angular/core';
import { BpObject } from '../models/bp_object';
import { BpReleaseReaderService } from './bp-release-reader.service'

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  constructor(private bpReleaseReader:BpReleaseReaderService) { }

  public get templates() : BpObject[]{
    return this.bpReleaseReader.getAllBpObjectTemplates();
  }

  public getObjectById(id: string): BpObject {
    return this.templates.find(f => f.id === id);
  }

  public getObjectIds(): string[] {
    return this.templates.map(item => item.id);
  }

  public getObjectsByName(name: string): BpObject[] {
    return this.templates.filter(item => item.name === name);
  }

  public getObjectFromSearchWord(searchWord: string): BpObject[] {
    return this.templates.filter(o => o.name.toLowerCase().includes(searchWord.toLowerCase()));
  }
  public getObjectPageFromSearchWord(searchWord: string): BpObject[] {
    let templates = this.templates;
    for(let i=0;i<templates.length;i++){
      templates[i].functions= templates[i].functions
        .filter(o => o.name.toLowerCase().includes(searchWord.toLowerCase()))
    }
    return templates.filter(o=>o.functions.length>0);
  }
}
