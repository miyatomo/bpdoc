import { Injectable, OnInit } from '@angular/core';
import { BpGroup } from '../models/bp_group';
import { BpObject } from '../models/bp_object';
import { BpReleaseFile, BpReleaseFileMeta } from '../models/bp_release_file';
import { AppConfig } from '../../environments/environment';
import * as fs from 'fs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BpReleaseReaderService implements OnInit {
  public releaseList: BpReleaseFile[]

  public displayBpObjects: { packageId: string, groups: BpGroup[], objects: BpObject[] }[] = [];

  constructor() { }

  ngOnInit(): void {
    this.fetchBpRelease()
  }

  public getAllGroupTemplates(): BpGroup[] {
    return this.releaseList.map(i => i.groups).reduce((prev, next) => {
      if (next) {
        next.concat(prev)
      } else {
        next = prev
      }
      return next
    });
  }

  public getAllBpObjectTemplates(): BpObject[] {
    return this.releaseList.map(i => i.objects).reduce((prev, next) => {
      if (next) {
        next.concat(prev)
      } else {
        next = prev
      }
      return next
    });
  }

  public addDisplayRelease(packageId: string) {
    const filteredReleaseList = this.releaseList.filter(i => i.meta.packageId === packageId)
    filteredReleaseList[0]
    
  }

  public fetchBpRelease()
  {
    const remote = require('electron').remote;
    const app = remote.app;
    const path = require('path');
    const inDir = path.join(app.getPath('appData'), "BPReleaseSources");
    const fileList = fs.readdirSync(inDir);

    fileList.forEach(file => {
      const data = fs.readFileSync(path.join(inDir, file), "utf8");
      const jsonObject: BpReleaseFile = JSON.parse(data);
      this.releaseList.push(jsonObject)
    });
    this.displayBpObjects =  
    [{packageId: this.releaseList[0].meta.packageId, 
      groups: this.releaseList[0].groups, 
      objects: this.releaseList[0].objects }];
  }
}
