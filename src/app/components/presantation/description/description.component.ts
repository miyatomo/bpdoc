import { Component, OnInit, Input } from '@angular/core';

// TODO:デザイン
@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent implements OnInit {

  @Input() description: string;
  @Input() preconditions: string[];
  @Input() postconditions: string[];

  constructor() { }

  ngOnInit() { }

}
