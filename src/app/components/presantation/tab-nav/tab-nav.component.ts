import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tab-nav',
  templateUrl: './tab-nav.component.html',
  styleUrls: ['./tab-nav.component.css']
})
export class TabNavComponent implements OnInit {

  @Output() changeTab: EventEmitter<string> = new EventEmitter();

  active = 'information';

  constructor() { }

  ngOnInit() {
  }

  handleClick(active: string) {
    this.active = active;
    this.changeTab.emit(this.active);
  }

}
