import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-menu-btn',
  templateUrl: './menu-btn.component.html',
  styleUrls: ['./menu-btn.component.css']
})
export class MenuBtnComponent implements OnInit {

  @Input() open: boolean = false;
  @Output() toggle: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  handleClick() {
    this.open = !this.open;
    if (this.open) {
      return 'open';
    } else {
      return 'close';
    }
  }
}
