import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BpObjectComponent } from './bp-object.component';

describe('BpObjectComponent', () => {
  let component: BpObjectComponent;
  let fixture: ComponentFixture<BpObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BpObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BpObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
