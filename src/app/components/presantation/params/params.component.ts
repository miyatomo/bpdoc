import { Component, OnInit, Input } from '@angular/core';
import { Param } from '../../../models/param';

@Component({
  selector: 'app-params',
  templateUrl: './params.component.html',
  styleUrls: ['./params.component.css']
})
export class ParamsComponent implements OnInit {

  @Input() category: string;
  @Input() params: Param[];

  constructor() { }
  ngOnInit() {}
}
