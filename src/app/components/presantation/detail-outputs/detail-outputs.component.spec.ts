import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailOutputsComponent } from './detail-outputs.component';

describe('DetailOutputsComponent', () => {
  let component: DetailOutputsComponent;
  let fixture: ComponentFixture<DetailOutputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailOutputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailOutputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
