import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SuggestMsg } from '../../../models/suggest';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  control = new FormControl('');
  @Output() suggest: EventEmitter<SuggestMsg[]> = new EventEmitter();

  constructor() { 

    this.control.valueChanges.pipe(debounceTime(200)).subscribe((searchString)=>
    {
      this.suggest.emit(searchString);
    })
  }

  ngOnInit() {
  }
}
