import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailInputsComponent } from './detail-inputs.component';

describe('DetailInputsComponent', () => {
  let component: DetailInputsComponent;
  let fixture: ComponentFixture<DetailInputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailInputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
