import { Component, OnInit, Input } from '@angular/core';
import UUID from 'uuid';
import { clipboard } from 'electron';

@Component({
  selector: 'app-copy-button',
  templateUrl: './copy-button.component.html',
  styleUrls: ['./copy-button.component.scss']
})
export class CopyButtonComponent implements OnInit {

  @Input() funcName: string;
  @Input() objName: string;

  constructor() { }

  ngOnInit() {
  }

  copyMessage(){
    const copyStr = `<process name="__selection__"><stage stageid="${UUID.v4()}" name="${this.funcName}" type="Action"><resource object="${this.objName}" action="${this.funcName}" /></stage></process>`;
    clipboard.writeText(copyStr);
  }
}
