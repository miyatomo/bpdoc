import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ObjectService } from '../../../services/object.service';
import { BpObject } from '../../../models/bp_object';
import { Router } from '@angular/router'
declare var M ;

//TODO:もしかしたらOverviewとかつけるかも
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input('open-nav') open: boolean;

  public searchResultObjects: BpObject[] = [];
  public searchResultPageObjects: BpObject[] = [];

  constructor(
    public router: Router,
    private bpObj: ObjectService
  ) { }

  ngOnInit() {
  }

  handleSuggest($event: string) {
    // 入力文字列がある場合はページ遷移する。入力文字列が変化した場合は検索結果をクリアする
    // TODO:検索前のページを保存して、結果削除時に戻すようにする
    if($event.length!==0) this.router.navigateByUrl(`searchResult`);
    if($event.length===0) this.router.navigateByUrl(`template`);
    if($event.length===0) return;
    this.searchResultObjects = this.bpObj.getObjectFromSearchWord($event);
    this.searchResultPageObjects = this.bpObj.getObjectPageFromSearchWord($event);
  }

  ngAfterViewInit(): void {
    setTimeout( function() {
      var elem = document.querySelectorAll('.sidenav');
      var instance = M.Sidenav.init(elem);
    }, 0)
  }

  ngOnDestroy() {
  }

}
