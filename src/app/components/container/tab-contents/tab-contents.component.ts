import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tab-contents',
  templateUrl: './tab-contents.component.html',
  styleUrls: ['./tab-contents.component.css']
})
export class TabContentsComponent implements OnInit {

  @Input('objFunctions') funcs: any;

  tab = 'information';

  constructor() { }

  ngOnInit() {
    
  }
}
