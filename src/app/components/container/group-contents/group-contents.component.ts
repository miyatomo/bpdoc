import { Component, OnInit } from '@angular/core';
import { BpGroup } from '../../../models/bp_group';
import { GroupService } from '../../../services/group.service';

//TODO:デザインやる
@Component({
  selector: 'app-group-contents',
  templateUrl: './group-contents.component.html',
  styleUrls: ['./group-contents.component.css']
})
export class GroupContentsComponent implements OnInit {

  public groups: BpGroup[];

  constructor(
    private grpService: GroupService
  ) { }

  ngOnInit() {
    this.groups = this.grpService.getAllGroup();
  }

}
