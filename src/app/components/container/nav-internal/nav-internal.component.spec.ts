import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavInternalComponent } from './nav-internal.component';

describe('NavInternalComponent', () => {
  let component: NavInternalComponent;
  let fixture: ComponentFixture<NavInternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavInternalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
