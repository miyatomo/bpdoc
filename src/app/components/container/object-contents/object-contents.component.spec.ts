import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectContentsComponent } from './object-contents.component';

describe('ObjectContentsComponent', () => {
  let component: ObjectContentsComponent;
  let fixture: ComponentFixture<ObjectContentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectContentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectContentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
