import { Component, OnInit } from '@angular/core';
import { ObjectService } from '../../../services/object.service';
import { BpObject } from '../../../models/bp_object';

// FIXME:これから作る
@Component({
  selector: 'app-object-contents',
  templateUrl: './object-contents.component.html',
  styleUrls: ['./object-contents.component.css']
})
export class ObjectContentsComponent implements OnInit {

  objectList: BpObject[] = [];

  constructor(
    private bpObj: ObjectService,
  ) { }

  ngOnInit() {
    this.objectList = this.bpObj.templates;
  }
}
