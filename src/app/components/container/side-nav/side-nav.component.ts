import { Component, OnInit, Input } from '@angular/core';
import { ObjectService } from '../../../services/object.service';

// 作り直し
@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {

  @Input() open: boolean = false;

  public sideNavs : {
    value: string,
    link: string
  }[];

  constructor(private objService: ObjectService) { }

  ngOnInit() {
    // 1. homeだったらそもそもsidenav表示せん
    
    // 2. groupsの時はsidenavはgroupリスト

    // 3. 特定のgroupの時はsidenavはObjectリスト
    
  }

}
