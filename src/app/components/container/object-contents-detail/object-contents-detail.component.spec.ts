import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectContentsDetailComponent } from './object-contents-detail.component';

describe('ObjectContentsDetailComponent', () => {
  let component: ObjectContentsDetailComponent;
  let fixture: ComponentFixture<ObjectContentsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectContentsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectContentsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
