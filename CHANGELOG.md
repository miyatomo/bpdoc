# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2019-10-18

### Added
* 作成したjsonがユーザフォルダに保存されるようにした
* アクションコピー機能を実装した

### Changed
* Object読み込み速度改善


## [0.1.0] - 2019-07-20

### Added
* 基本機能の実装

[1.0.0]: 
[0.1.0]: https://gitlab.com/miyatomo/bpdoc/-/tags/v0.1
